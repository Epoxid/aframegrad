#include "AFrameNURBS.h"
#include "AImage.h"

#include <sstream>
std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str)
{
    std::vector<std::string>   result;
    std::string                line;
    std::getline(str,line);

    std::stringstream          lineStream(line);
    std::string                cell;

    while(std::getline(lineStream,cell, ','))
    {
        result.push_back(cell);
    }
    // This checks for a trailing comma with no data after it.
    if (!lineStream && cell.empty())
    {
        // If there was a trailing comma then add an empty element.
        result.push_back("");
    }
    return result;
}

int main()
{
    //std::vector<double> t = {-2, -2, -2, -2, -1, 0, 1, 2, 2, 2, 2};
    //std::vector<double> t = {0,1,2,3,4,5};
    //std::vector<double> t = {0,0,0,0,1,2,3,4,4,4,4};
    
    //double f = SplineA.Eval(-1.0, order);
    //std::cout << std::scientific << "ans: " << f << std::endl;
    
    //SplineA.WriteCSV("spline.csv", -2.0, 2.0, 1024, order);
    //SplineA.WriteBSplineN("Basis0.csv", -1.0, 6.0, 2048);
    
    /*
    std::vector<double> dataR;
    std::vector<double> dataG;
    std::vector<double> dataB;
    std::ifstream inputfile("flat.csv");

    while(!inputfile.eof())
    {
        std::vector<std::string> line = getNextLineAndSplitIntoTokens(inputfile);
        if(line[0] != "")
        {
            dataR.push_back(stod(line[0]));
            dataG.push_back(stod(line[1]));
            dataB.push_back(stod(line[2]));
        }
    }
    std::cout << "File contrains " << dataR.size() << " entries" << std::endl;
    
    const int order = 3;
    const int KnotDist = 200;//dataR.size()/10;
    const int NumKnots = dataR.size()/KnotDist;
    std::vector<double> Knots;
    for(int i=0;i<NumKnots;i++) Knots.push_back(i*KnotDist);
    Knots.push_back(dataR.size());
    
    //positions for fit
    std::vector<double> Positions;
    for(int i=0;i<dataR.size();i++) Positions.push_back(i);
    
    AFrameBSpline<double> SplineA(Knots, order);
    SplineA.WriteBSplineN("Basis0.csv", 0, dataR.size(), 2048);
    
    SplineA.LinearFit(Positions, dataR);
    SplineA.WriteCSV("spline.csv", 0, dataR.size(), 10*(dataR.size()));
    */
    
    std::cout << "START" << std::endl;
    
    std::string Path = "./";
    std::string NameFIT = "Iris_1_R6.fits";
    std::string NameFlat = "Master_Flat_RGB.fit";
    
    AFrameBSpline<float> Spline1;
    
    AImage<float> Frame;
    Frame.LoadFits(Path+NameFIT);
    Frame.WriteTIFF("light.tif");

    std::cout << "Image is " << Frame.sizeX << " x " << Frame.sizeY << " pixels" << std::endl;
    
    const uint16_t order = 2;
    //results in NumKnots-1 panels
    const uint32_t NumKnots = 2;
    const uint32_t StrideX = Frame.sizeX/(NumKnots-1);
    const uint32_t StrideY = Frame.sizeY/(NumKnots-1);
    
    std::cout << "StrideX: " << StrideX <<", StrideY " << StrideY << std::endl;
    std::vector<float> KnotsX(NumKnots);
    for(int i=0;i<NumKnots-1;i++)
    {
        KnotsX[i] = i*StrideX;
    }
    //add last pixel
    KnotsX[NumKnots-1] = Frame.sizeX-1;
    
    std::vector<float> KnotsY(NumKnots);
    for(int i=0;i<NumKnots-1;i++)
    {
        KnotsY[i] = i*StrideY;
    }
    //add last pixel
    KnotsY[NumKnots-1] = Frame.sizeY-1;
    
    std::cout << "Knots created" << std::endl;
    
    AFrameBSplineSurface<float>* BackgroundSplineSurface = new AFrameBSplineSurface<float>[3];
    for(int c=0;c<3;c++)
    {
        std::cout << "Making Background for Channel " << c << std::endl;
        BackgroundSplineSurface[c] = AFrameBSplineSurface<float>(KnotsX, KnotsY, order, order);
        
        const uint32_t NumDataPoints = 100;
        const uint32_t DataStrideX = Frame.sizeX/(NumDataPoints-1);
        const uint32_t DataStrideY = Frame.sizeY/(NumDataPoints-1);
        std::vector< std::vector< std::vector<float> > > Positions(NumDataPoints, std::vector< std::vector<float> >(NumDataPoints,std::vector<float>(2)));
        std::vector< std::vector<float> > Data(NumDataPoints, std::vector<float>(NumDataPoints, 1.0));
        for(int j=0;j<NumDataPoints;j++)
        {
            const int32_t y = (j)*DataStrideY;
            for(int i=0;i<NumDataPoints;i++)
            {
                //std::cout << "j = " << j << ", i = " << i << std::endl;
                const int32_t x = (i)*DataStrideX;
                //compute mean
                float avg = 0.0;//[3] = {0.0, 0.0, 0.0};
                float divisor = 0.0;//[3] = {0.0, 0.0, 0.0};
                const long delta = 5;
                for(long jj=-delta;jj<delta;jj++)
                {
                    for(long ii=-delta;ii<delta;ii++)
                    {
                        if(x+ii >= 0 && x+ii < Frame.sizeX && y+jj >= 0 && y+jj < Frame.sizeY)
                        {
                            avg += Frame.GetValue(x, y, c);
                            divisor += 1;
                        }
                    }
                }
                avg = avg/divisor;
                //reject if more than 50% from mean
                float value = 0.0;
                divisor = 0.0;
                for(long jj=-delta;jj<delta;jj++)
                {
                    for(long ii=-delta;ii<delta;ii++)
                    {
                        if(x+ii >= 0 && x+ii < Frame.sizeX && y+jj >= 0 && y+jj < Frame.sizeY)
                        {
                            const float tmp = Frame.GetValue(x, y, c);
                            if(fabs(tmp-avg)/avg < 0.75)
                            {
                                value += tmp;
                                divisor += 1;
                            }
                        }
                    }
                }
                //Data[j][i] = Frame.GetValue(x, y, c);
                Data[j][i] = value/divisor;
                Positions[j][i][0] = (float) x;
                Positions[j][i][1] = (float) y;
            }
        }
        std::cout << "Start Linear Fit" << std::endl;
        BackgroundSplineSurface[c].LinearFit(Positions, Data);
    }
    std::cout << "All Channels interpolated" << std::endl;
    AImage<float> BackgroundImage(Frame.sizeX, Frame.sizeY, 3);
    float mean[3] = {0.0,0.0,0.0};
    for(int c=0;c<3;c++)
    {
        std::cout << "channel: " << c << std::endl;
        #pragma omp parallel for
        for(unsigned long j = 0; j<BackgroundImage.sizeY; j++)
        {
            //std::cout << "channel: " << c << ", j: " << j << std::endl;
            for(unsigned long i = 0; i<BackgroundImage.sizeX; i++)
            {
                const float value = BackgroundSplineSurface[c].Eval((double) i, (double) j);
                //std::cout << value << std::endl;
                BackgroundImage.SetValue(i,j,c,value);
                mean[c] += value;
            }
        }
    }
    std::cout << "Writing Tiff" << std::endl;
    BackgroundImage.WriteTIFF("BackgroundSpline.tif");
    
    AImage<float> ImageSubstracted(Frame.sizeX, Frame.sizeY, 3);
    for(int c=0;c<3;c++)
    {
        #pragma omp parallel for
        for(unsigned long j = 0; j<BackgroundImage.sizeY; j++)
        {
            //std::cout << "channel: " << c << ", j: " << j << std::endl;
            for(unsigned long i = 0; i<BackgroundImage.sizeX; i++)
            {
                const float image = Frame.GetValue(i,j,c);
                const float background = BackgroundImage.GetValue(i,j,c);
                ImageSubstracted.SetValue(i,j,c, image+(mean[c]/(BackgroundImage.sizeY*BackgroundImage.sizeX))-background);
            }
        }
    }
    ImageSubstracted.WriteTIFF("BackgroundSubtracted.tif");
    
    delete [] BackgroundSplineSurface;
    std::cout << "ENDE" <<std::endl;
    return 0;
}
