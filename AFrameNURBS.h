#include <iostream>
#include <fstream>
#include <vector>

template <class REAL>
class AFrameBSpline
{
private:
    uint16_t NumKnots;
    //weights for linear combination for evaluation
    REAL *Coefficients = NULL;
    REAL GetW(const REAL x, const uint16_t i, const uint16_t k)
    {
#ifdef DEBUG
        const unsigned long length = t.size();
        if(i+k >= length)
        {
            std::cerr << "GetW: Index out of bounds" << std::endl;
            exit(1);
            return 0.0;
        }
#endif
        if(t[i+k] != t[i]) return (x-t[i])/(t[i+k]-t[i]);
        else return 0.0;
    }

public:
    uint16_t order;
    uint16_t NumBasis;
    //knots/control points
    std::vector<REAL> t;
    AFrameBSpline()
    {
        //std::cout << "Default constructor" <<std::endl;
        Coefficients = NULL;
        t = std::vector<REAL>(0);
    }
    AFrameBSpline(std::vector<REAL> &t_in, const uint16_t order_in)
    {
        order = order_in;
        t = t_in;
        NumKnots = t.size()+1;
        NumBasis = t.size() + order - 1;
        const REAL v1 = t[0];
        for(int i=0;i<order;i++) t.insert(t.begin(), v1);
        const REAL v2 = t[t.size()-1];
        for(int i=0;i<order;i++) t.insert(t.end(), v2);
        Coefficients = new REAL[NumBasis];
    }
    ~AFrameBSpline()
    {
        //std::cout << "Default DEstructor" <<std::endl;
        if(Coefficients != NULL) delete [] Coefficients;
    }
    //assignment
    AFrameBSpline& operator=(AFrameBSpline other)
    {
        //std::cout << "BSpline Assignment Operator of A\n";
        order = other.order;
        NumKnots = other.NumKnots;
        NumBasis = other.NumBasis;
        //weights for linear combination for evaluation
        if(Coefficients != NULL) delete [] Coefficients;
        Coefficients = NULL;
        if(other.Coefficients != NULL)
        {
            Coefficients = new REAL[NumBasis];
            for(int i=0;i<NumBasis;i++)
            {
                Coefficients[i] = other.Coefficients[i];
            }
        }
        t = other.t;
        return *this;
    }
    AFrameBSpline(const AFrameBSpline& other)
    {
        order = other.order;
        NumKnots = other.NumKnots;
        NumBasis = other.NumBasis;
        //weights for linear combination for evaluation
        if(Coefficients != NULL) delete [] Coefficients;
        Coefficients = NULL;
        if(other.Coefficients != NULL)
        {
            Coefficients = new REAL[NumBasis];
            for(int i=0;i<NumBasis;i++)
            {
                Coefficients[i] = other.Coefficients[i];
            }
        }
        t = other.t;
    }
    REAL EvalBSplineN(const REAL x, const uint16_t i, const uint16_t k)
    {
#ifdef DEBUG
        const unsigned long length = t.size();
        if(i+k >= length)
        {
            std::cerr << "EvalBSpine: i= " << i << ", k= " << k << " out of bounds, probably too high degree" << std::endl;
            exit(1);
        }
#endif
        if(k==0)
        {
            //sanity check at last point
            if(i == NumBasis-1 && x == t[t.size()-1]) return 1.0;
            if(t[i] <= x && x < t[i+1]) return 1.0;
            else return 0.0;
        }
        else
        {
            const REAL wi0k = GetW(x, i, k);
            const REAL wi1k = GetW(x, i+1, k);
            const REAL Bi0k = EvalBSplineN(x, i, k-1);
            const REAL Bi1k = EvalBSplineN(x, i+1, k-1);
            //std::cout << "Eval to " << x << " " << wi0k*Bi0k + (1.0-wi1k)*Bi1k <<std::endl;
            return wi0k*Bi0k + (1.0-wi1k)*Bi1k;
        }
    }
    REAL Eval(const REAL x)
    {
        REAL ans = 0.0;
        for(int i=0;i<NumBasis;i++) ans += Coefficients[i]*EvalBSplineN(x, i, order);
        return ans;
    }
    inline void SetWeight(int &id, REAL value)
    {
        Coefficients[id] = value;
    }
    inline REAL GetWeight(int &id)
    {
        return Coefficients[id];
    }
    //write all basis function of deg k to file
    int WriteBSplineN(std::string FileName, const REAL start, const REAL end, const uint16_t steps)
    {
        const uint16_t k = order;
        std::ofstream outfile;
        outfile.open(FileName);
        outfile << std::scientific;
        const REAL delta = (end-start)/((REAL) (steps-1));
        for(int i=0; i<steps;i++)
        {
            const REAL x = start + (REAL)i*delta;
            outfile << x;
            for(int i=0;i<NumBasis;i++)
            {
                //std::cout << i <<std::endl;
                const REAL value = EvalBSplineN(x, i, k);
                outfile << ","<<value;
            }
            outfile << std::endl;
        }
        outfile.close();
        return 0;
    }
    int WriteCSV(std::string FileName, const REAL start, const REAL end, const uint16_t steps)
    {
        std::ofstream outfile;
        outfile.open(FileName);
        outfile << std::scientific;
        const REAL delta = (end-start)/((REAL) (steps-1));
        for(int i=0; i<steps;i++)
        {
            const REAL x = start + (REAL)i*delta;
            const REAL value = Eval(x);
            outfile << x << ","<<value<<std::endl;
        }
        outfile.close();
        return 0;
    }
    int LinearFit(std::vector<REAL> &Positions, std::vector<REAL> &values);
};

#include "gsl/gsl_multifit.h"
template <class REAL>
int AFrameBSpline<REAL>::LinearFit(std::vector<REAL> &Positions, std::vector<REAL> &values)
{
#ifdef DEBUG
    if(Positions.size() != values.size())
    {
        std::cerr << "Error linear fit, size mismatch"<<std::endl;
        exit(1);
        return(1);
    }
    if(Positions.size() < NumBasis)
    {
        std::cerr << "Error linear fit, not enough data for fit" <<std::endl;
        exit(1);
    }
#endif
    gsl_matrix *A;
    gsl_matrix *cov;
    gsl_vector *b;
    gsl_vector *w;
    //result vector
    gsl_vector *ans;
    A = gsl_matrix_alloc(Positions.size(), NumBasis);
    cov = gsl_matrix_alloc(NumBasis, NumBasis);
    b = gsl_vector_alloc(Positions.size());
    //result vector
    ans = gsl_vector_alloc(NumBasis);
    w = gsl_vector_alloc(Positions.size());
    for(int i=0;i<Positions.size();i++)
    {
        for(int basis=0;basis<NumBasis;basis++)
        {
            gsl_matrix_set(A, i, basis, EvalBSplineN(Positions[i], basis, order));
        }
        gsl_vector_set(b, i, values[i]);
        gsl_vector_set(w, i, 1.0);
    }
    gsl_multifit_linear_workspace* work = gsl_multifit_linear_alloc(Positions.size(), NumBasis);
    double chisq;
    gsl_multifit_wlinear(A, w, b, ans, cov, &chisq, work);
    gsl_multifit_linear_free(work);
    for(int basis=0;basis<NumBasis;basis++)
    {
        SetWeight(basis, gsl_vector_get(ans, basis));
    }
    gsl_matrix_free(A);
    gsl_matrix_free(cov);
    gsl_vector_free(b);
    gsl_vector_free(w);
    gsl_vector_free(ans);
    return 0;
}

template <class REAL>
class AFrameBSplineSurface
{
private:
    REAL **Coefficients = NULL;
    AFrameBSpline<REAL> BSplineX;
    AFrameBSpline<REAL> BSplineY;
public:
    AFrameBSplineSurface()
    {
        Coefficients = NULL;
        BSplineX = AFrameBSpline<REAL>();
        BSplineY = AFrameBSpline<REAL>();
    }
    AFrameBSplineSurface(std::vector<REAL> KnotsX, std::vector<REAL> KnotsY, const uint16_t orderXin, const uint16_t orderYin)
    {
        BSplineX = AFrameBSpline<REAL>(KnotsX, orderXin);
        BSplineY = AFrameBSpline<REAL>(KnotsY, orderYin);
        Coefficients = new REAL*[BSplineY.NumBasis];
        for(int j=0;j<BSplineY.NumBasis;j++)
        {
            Coefficients[j] = new REAL[BSplineX.NumBasis];
        }
    }
    ~AFrameBSplineSurface()
    {
        if(Coefficients != NULL)
        {
            for(int j=0;j<BSplineY.NumBasis;j++) delete [] Coefficients[j];
            delete [] Coefficients;
        }
    }
    AFrameBSplineSurface& operator=(AFrameBSplineSurface other)
    {
        std::cout << "Assignment called" << std::endl;
        if(Coefficients != NULL) delete [] Coefficients;
        BSplineX = other.BSplineX;
        BSplineY = other.BSplineY;
        if(other.Coefficients != NULL)
        {
            Coefficients = new REAL*[BSplineY.NumBasis];
            for(int j=0;j<BSplineY.NumBasis;j++)
            {
                Coefficients[j] = new REAL[BSplineX.NumBasis];
            }
        }
        return *this;
    }
    AFrameBSplineSurface(const AFrameBSplineSurface& other)
    {
        std::cout << "Copy Surface called " << std::endl;
        exit(1);
    }
    REAL Eval(const double x, const double y)
    {
        REAL ans = 0.0;
        for(int j=0;j<BSplineY.NumBasis;j++)
        {
            const REAL vY = BSplineY.EvalBSplineN(y, j, BSplineY.order);
            for(int i=0;i<BSplineX.NumBasis;i++)
            {
                const REAL vX = BSplineX.EvalBSplineN(x, i, BSplineX.order);
                ans += Coefficients[j][i]*vY*vX;
            }
        }
        return ans;
    }
    inline void SetWeight(const uint32_t i, const uint32_t j, REAL value)
    {
        Coefficients[j][i] = value;
    }
    int LinearFit(std::vector< std::vector< std::vector<REAL> > > Positions, std::vector< std::vector<REAL> > Values)
    {
        const uint16_t NumUnknown = BSplineY.NumBasis*BSplineX.NumBasis;
        const uint16_t NumData = Values.size()*Values[0].size();
        /*
        std::cout << Values.size() << " " << Values[0].size() << std::endl;
        std::cout << "Number of basis functions " << NumUnknown << std::endl;
        std::cout << "Number of data points " << NumData << std::endl;
        */
    #ifdef DEBUG
        if(Positions.size() != Values.size())
        {
            std::cerr << "Error linear fit, size y mismatch"<<std::endl;
            exit(1);
            return(1);
        }
        for(int j=0;j<Positions.size();j++)
        {
            if(Positions[j].size() != Values[j].size())
            {
                std::cerr << "Error linear fit, size x mismatch"<<std::endl;
                exit(1);
                return(1);
            }
        }
        if(NumData < NumUnknown)
        {
            std::cerr << "Error linear fit, not enough data for fit" <<std::endl;
            std::cerr << "Number of basis functions " << NumUnknown << std::endl;
            std::cerr << "Number of data points " << NumData << std::endl;
            exit(1);
        }
    #endif
        gsl_matrix *A;
        gsl_matrix *cov;
        gsl_vector *b;
        gsl_vector *w;
        //result vector
        gsl_vector *ans;
        
        A = gsl_matrix_alloc(NumData, NumUnknown);
        cov = gsl_matrix_alloc(NumUnknown, NumUnknown);
        b = gsl_vector_alloc(NumData);
        //result vector
        ans = gsl_vector_alloc(NumUnknown);
        w = gsl_vector_alloc(NumData);
        std::cout << "creating gsl data" << std::endl;
        #pragma omp parallel for
        for(unsigned long j=0;j<Positions.size();j++)
        {
            for(unsigned long i=0;i<Positions[j].size();i++)
            {
                const unsigned long IndexData = j*Positions[j].size() + i;
                if(IndexData >= NumData) std::cout << "problem" << std::endl;
                
                for(int unknownY=0;unknownY<BSplineY.NumBasis;unknownY++)
                {
                    const REAL BasisValueY = BSplineY.EvalBSplineN(Positions[j][i][1], unknownY, BSplineY.order);
                    for(int unknownX=0;unknownX<BSplineX.NumBasis;unknownX++)
                    {
                        const REAL BasisValueX = BSplineX.EvalBSplineN(Positions[j][i][0], unknownX, BSplineX.order);
                        const uint32_t IndexUnknown = unknownY*BSplineX.NumBasis + unknownX;
                        
                        const REAL BasisValues = BasisValueY*BasisValueX;
                    
                        //std::cout << "Setting Data: " << IndexData << ", Unknown: " << IndexUnknown << std::endl;
                        gsl_matrix_set(A, IndexData, IndexUnknown, BasisValues);
                    }
                }
                gsl_vector_set(b, IndexData, Values[j][i]);
                gsl_vector_set(w, IndexData, 1.0);
            }
        }
        //std::cout << "test2" << std::endl;
        gsl_multifit_linear_workspace* work = gsl_multifit_linear_alloc(NumData, NumUnknown);
        double chisq;
        std::cout << "gsl solve" << std::endl;
        gsl_multifit_wlinear(A, w, b, ans, cov, &chisq, work);
        gsl_multifit_linear_free(work);
        //std::cout << "test3" << std::endl;
        for(int unknownY=0;unknownY<BSplineY.NumBasis;unknownY++)
        {
            for(int unknownX=0;unknownX<BSplineX.NumBasis;unknownX++)
            {
                const uint32_t IndexUnknown = unknownY*BSplineX.NumBasis + unknownX;
                SetWeight(unknownY, unknownX , gsl_vector_get(ans, IndexUnknown));
            }
        }
        gsl_matrix_free(A);
        gsl_matrix_free(cov);
        gsl_vector_free(b);
        gsl_vector_free(w);
        gsl_vector_free(ans);
        return 0;
    }
};
