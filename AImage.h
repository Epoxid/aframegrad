#include <string>
#include <iostream>

#include "fitsio.h"

template <class REAL>
class AImage
{
    public:
        uint8_t NumChannel;
        uint32_t sizeX;
        uint32_t sizeY;
        REAL ***ChannelData;
        AImage()
        {
            NumChannel = 0;
            sizeX = 0;
            sizeY = 0;
            ChannelData = NULL;
        }
        AImage(const uint32_t x, const uint32_t y, const uint8_t c)
        {
            sizeX = x;
            sizeY = y;
            NumChannel = c;
            //std::cout << "Init with NumChannel " << (uint16_t) NumChannel << std::endl;
            ChannelData = new REAL**[NumChannel];
            for(uint8_t c=0;c<NumChannel;c++)
            {
                ChannelData[c] = new REAL*[sizeY];
                for(uint32_t y=0;y<sizeY;y++)
                {
                    ChannelData[c][y] = new REAL[sizeX];
                }
            }
        }
        //Sorting B
        ~AImage()
        {
            if(ChannelData != NULL)
            {
                for(uint32_t c = 0; c<NumChannel;c++)
                {
                    for(uint32_t y = 0;y<sizeY; y++) delete [] ChannelData[c][y];
                    delete [] ChannelData[c];
                }
                delete [] ChannelData;
            }
        }
        inline void SetValue(const uint32_t x, const uint32_t y, const uint8_t c, const REAL value)
        {
            ChannelData[c][y][x] = value;
        }
        inline REAL GetValue(const uint32_t x, const uint32_t y, const uint8_t c)
        {
#ifdef DEBUG
            if(x>sizeX)
            {
                std::cerr << "GetValue: x = " << x << " out of range" << std::endl;
                exit(1);
            }
            if(y>sizeY)
            {
                std::cerr << "GetValue: y = " << y << " out of range" << std::endl;
                exit(1);
            }
            if(c > NumChannel)
            {
                std::cerr << "GetValue: Channel = " << c << " out of range" << std::endl;
                exit(1);
            }
            if(ChannelData == NULL)
            {
                std::cerr << "GetValue: Channel Data is NULL, Image not initialized" << std::endl;
                exit(1);
            }
#endif
            return ChannelData[c][y][x];
        }
        int SetRow(REAL* row, const uint32_t y, const uint8_t c)
        {
            if(ChannelData == NULL)
            {
                std::cerr << "Error setting row, image not initialized" << std::endl;
                return 1;
            }
            #pragma omp parallel for
            for(uint32_t x = 0; x<sizeX; x++)
            {
                ChannelData[c][y][x] = row[x];
            }
            return 0;
        }
        REAL* GetRow(const uint32_t y, uint8_t c)
        {
            return ChannelData[c][y];
        }
        int LoadFits(std::string);
        int WriteTIFF(std::string);
        int WriteLine(std::string, uint32_t, uint8_t);
};

/*
std::string GetFITSType(int Type)
{
    if(Type == BYTE_IMG) return "BYTE_IMG";
    if(Type == SHORT_IMG) return "SHORT_IMG";
    if(Type == LONG_IMG) return "LONG_IMG";
    if(Type == LONGLONG_IMG) return "LONGLONG_IMG";
    if(Type == FLOAT_IMG) return "FLOAT_IMG";
    if(Type == DOUBLE_IMG) return "DOUBLE_IMG";
    return "UNKNOWN_IMG";
}
*/

#include <fstream>

template <class REAL>
int AImage<REAL>::WriteLine(std::string FileName, uint32_t index, uint8_t axis)
{
    std::ofstream outfile;
    outfile.open(FileName);
    outfile << std::scientific;
    if(axis==0)
    {
        for(uint32_t y=0;y<sizeY;y++)
        {
            outfile << GetValue(index,y,0);
            for(uint8_t c = 1;c<NumChannel;c++)
            {
                outfile << "," << GetValue(index,y,c);
            }
            outfile << std::endl;
        }
    }
    if(axis==1)
    {
        for(uint32_t x=0;x<sizeX;x++)
        {
            outfile << GetValue(x,index,0);
            for(uint8_t c = 1;c<NumChannel;c++)
            {
                outfile << "," << GetValue(x,index,c);
            }
            outfile << std::endl;
        }
    }
    outfile.close();
    return 0;
}

template <class REAL>
int AImage<REAL>::LoadFits(std::string FileName)
{
    const char *cStringName = FileName.c_str();
    //std::cout << "Opening file "<<cStringName<<std::endl;
    
    fitsfile *fptr;
    int status = 0;
    fits_open_file(&fptr, cStringName, READONLY, &status);
    if (status) {
       fits_report_error(stderr, status); /* print error message */
       return(status);
    }
    //get bit depth
    //int bitpix;
    //fits_get_img_type(fptr, &bitpix, &status);
    //std::cout<<GetFITSType(bitpix)<<std::endl;
    //get dimensions
    int axis;
    fits_get_img_dim(fptr, &axis, &status);  /* read dimensions */
    //std::cout << "Axis: "<<axis<<std::endl;
    //get pixels
    long naxes[axis];
    fits_get_img_size(fptr, axis, naxes, &status);
    /*
    for(int i=0;i<axis;i++)
    {
        std::cout << naxes[i] << " ";
    }
    std::cout<<std::endl;
    */
    
    //Init class
    sizeX = (uint32_t) naxes[0];
    sizeY = (uint32_t) naxes[1];
    NumChannel = naxes[2];
    //std::cout << "Init with NumChannel " << (uint16_t) NumChannel << std::endl;
    ChannelData = new REAL**[NumChannel];
    for(uint8_t c=0;c<NumChannel;c++)
    {
        ChannelData[c] = new REAL*[sizeY];
        for(uint32_t y=0;y<sizeY;y++)
        {
            ChannelData[c][y] = new REAL[sizeX];
        }
    }
    
    //const uint32_t npixels = sizeX;  /* no. of pixels to read in each row */
    REAL *pix = new REAL [sizeX];
    int FitsDataType;
    if(std::is_same<REAL, double>::value) FitsDataType = TDOUBLE;
    if(std::is_same<REAL, float>::value) FitsDataType = TFLOAT;
    long firstpix[3] = {1,1,1};
    for (firstpix[2] = 1; firstpix[2] <= NumChannel; firstpix[2]++) //loop over RGB
    {
        for (firstpix[1] = 1; firstpix[1] <= sizeY; firstpix[1]++) //loop over rows
        {
            //std::cout << "fistpix1: " << firstpix[1] << " fistpix2: " << firstpix[2] << std::endl;
            /* Read images as doubles, regardless of actual datatype.  */
            /* Give starting pixel coordinate and no. of pixels to read.    */
            /* This version does not support undefined pixels in the image. */
            //if (fits_read_pix(fptr, FitsDataType, firstpix, sizeX, NULL, pix, NULL, &status))
            if (fits_read_pix(fptr, FitsDataType, firstpix, sizeX, NULL, pix, NULL, &status))
            {
                return 1;
                break;
            }
            SetRow(pix, (uint32_t)(firstpix[1]-1), (uint32_t)(firstpix[2]-1));
        }
    }
    delete [] pix;
    return 0;
}

#include "tiffio.h"
template <class REAL>
int AImage<REAL>::WriteTIFF(std::string FileName)
{
    const char *cStringName = FileName.c_str();
    TIFF *image = TIFFOpen(cStringName, "w");
    TIFFSetField(image, TIFFTAG_IMAGEWIDTH, sizeX);
    TIFFSetField(image, TIFFTAG_IMAGELENGTH, sizeY);
    if(std::is_same<REAL, double>::value)
    {
        TIFFSetField(image, TIFFTAG_BITSPERSAMPLE, 64);
        TIFFSetField(image, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
    }
    else if(std::is_same<REAL, float>::value)
    {
        TIFFSetField(image, TIFFTAG_BITSPERSAMPLE, 32);
        TIFFSetField(image, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
    }
    else
    {
        TIFFSetField(image, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
        TIFFSetField(image, TIFFTAG_BITSPERSAMPLE, 32);
        std::cerr << "Write TIFF unknown bit format"<<std::endl;
        return 1;
    }
    //TIFFSetField(image, TIFFTAG_STRIPOFFSETS, 32);
    //TIFFSetField(image, TIFFTAG_DATATYPE, 16);
    
    TIFFSetField(image, TIFFTAG_SAMPLESPERPIXEL, NumChannel);
    if(NumChannel == 1)
    {
        TIFFSetField(image, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    }
    else
    {
        TIFFSetField(image, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
    }
    //TIFFSetField(image, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(image, TIFFTAG_PLANARCONFIG, PLANARCONFIG_SEPARATE);
    
    
    TIFFSetField(image, TIFFTAG_ROWSPERSTRIP, 1);
    //TIFFSetField(image, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField(image, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT);
    TIFFSetField(image, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
    TIFFSetField(image, TIFFTAG_RESOLUTIONUNIT, RESUNIT_NONE);

    //std::cout << sizeX << " " << sizeY << " " << NumChannel<< "blupp"<<std::endl;
    
    for(uint8_t c=0;c<NumChannel;c++)
    {
        //std::cout << "Writing Channel: " << (uint16_t) c<<std::endl;
        for (uint32_t y = 0; y < sizeY; y++)
        {
            REAL *Data = GetRow(y,c);
            TIFFWriteScanline(image, Data, y, c);
        }
    }
    TIFFClose(image);
    return 0;
}
